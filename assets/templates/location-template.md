# Vermilion City
- [Vermilion City](#vermilion-city)
- [NPCs](#npcs)
  - [Pokemon Fan Club President Martin](#pokemon-fan-club-president-martin)
    - [Description](#description)
  - [Mason](#mason)
    - [Description](#description-1)
- [Trainers](#trainers)
  - [Lt. Surge](#lt-surge)
    - [Rewards](#rewards)
    - [Party](#party)
  - [Engineer Lincoln](#engineer-lincoln)
    - [Rewards](#rewards-1)
    - [Party](#party-1)
  - [Sailor Charles](#sailor-charles)
    - [Rewards](#rewards-2)
    - [Party](#party-2)
  - [Engineer Roman](#engineer-roman)
    - [Rewards](#rewards-3)
    - [Party](#party-3)
# NPCs
## Pokemon Fan Club President Martin
![npcImage](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Trainer%20Sprites/Gentleman2.png)

*Location: Vermilion City House*
### Description
If you have caught more than 15 kinds of Pokemon, he gives you a Running Shoes Pass. You can take this pass to Cerulean City to the Bike Shop to get some running shoes.
## Mason
![npcImage](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Trainer%20Sprites/JonahSprite.png)

*Location: Vermilion City House*
### Description
Offers to trade you his Farfetch'd for a Fearow.

 | Party                                                                                                       | Pokemon           | Level | Special Moveset |
 | ----------------------------------------------------------------------------------------------------------- | ----------------- | ----- | --------------- |
 | ![pokemonParty1](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Sprites/83.png) | **#83** Farfetchd | 10    | Default         |

# Trainers
## Lt. Surge
![trainerImage](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Trainer%20Sprites/LtSurge.png)

*Location: Vermilion City Gym*
### Rewards
- Thunder Badge
- TM - Thunderbolt
### Party
 | Party                                                                                                        | Pokemon          | Level   | Special Moveset                                                        |
 | ------------------------------------------------------------------------------------------------------------ | ---------------- | ------- | ---------------------------------------------------------------------- |
 | ![pokemonParty1](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Sprites/100.png) | **#100** Voltorb | PL^ - 3 | Thunder Wave, Thunderbolt, Hidden Power, Swift                         |
 | ![pokemonParty2](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Sprites/25.png)  | **#25** Pikachu  | PL^ - 3 | Thunder Wave, Thunderbolt, Agility, Swiftm Brick Break, Body Slam      |
 | ![pokemonParty3](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Sprites/26.png)  | **#26** Raichu   | PL^ + 1 | Thunder Wave, Thunderbolt, Agility, Body Slam, Brick Break, Zap Cannon |
 
 *PL^ - Highest Level Pokemon in your Party**

 ## Engineer Lincoln
![trainerImage](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Trainer%20Sprites/Engineer1.png)

*Location: Vermilion City Gym*

### Rewards
- $ 500
### Party
 | Party                                                                                                        | Pokemon          | Level | Special Moveset |
 | ------------------------------------------------------------------------------------------------------------ | ---------------- | ----- | --------------- |
 | ![pokemonParty1](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Sprites/100.png) | **#100** Voltorb | 21    | Default         |
 | ![pokemonParty2](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Sprites/100.png) | **#100** Voltorb | 21    | Default         |
 | ![pokemonParty3](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Sprites/100.png) | **#100** Voltorb | 21    | Default         |

 ## Sailor Charles
![trainerImage](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Trainer%20Sprites/Sailor.png)

*Location: Vermilion City Gym*
### Rewards
- $720
### Party
 | Party                                                                                                       | Pokemon           | Level | Special Moveset |
 | ----------------------------------------------------------------------------------------------------------- | ----------------- | ----- | --------------- |
 | ![pokemonParty1](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Sprites/81.png) | **#81** Magnemite | 24    | Default         |
 
 ## Engineer Roman
![trainerImage](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Trainer%20Sprites/Engineer1.png)

*Location: Vermilion City Gym*
### Rewards
- $1540
### Party
 | Party                                                                                                        | Pokemon           | Level | Special Moveset                                                   |
 | ------------------------------------------------------------------------------------------------------------ | ----------------- | ----- | ----------------------------------------------------------------- |
 | ![pokemonParty1](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Sprites/100.png) | **#100** Voltorb  | 22    | Thunder Wave, Thunderbolt, Hidden Power, Swift                    |
 | ![pokemonParty2](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Sprites/81.png)  | **#81** Magnemite | 22    | Thunder Wave, Thunderbolt, Agility, Swiftm Brick Break, Body Slam |
