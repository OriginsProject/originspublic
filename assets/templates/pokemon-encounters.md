# Route 10
Route 10 is located east of Route 9 and leads to the Rock Tunnel

- [Route 10](#route-10)
- [Pokemon](#pokemon)
  - [Day](#day)
  - [Night](#night)
  - [Fishing](#fishing)
- [NPCs](#npcs)
  - [Engineer Lincoln](#engineer-lincoln)
    - [Rewards](#rewards)
    - [Party](#party)

# Pokemon
## Day
The following pokemon will spawn on Route 10 during the day.

| Pokemon            | Image                                                                                                       | Area  | Rate  | Level |
| ------------------ | ----------------------------------------------------------------------------------------------------------- | ----- | ----- | ----- |
| **# 21** Spearow   | ![pokemonImage](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Sprites/21.png)  | Grass | *15%* | 13-17 |
| **# 23** Ekans     | ![pokemonImage](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Sprites/23.png)  | Grass | *15%* | 11-17 |
| **# 100** Voltorb  | ![pokemonImage](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Sprites/100.png) | Grass | *45%* | 14-17 |
| **# 27** Sandshrew | ![pokemonImage](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Sprites/27.png)  | Grass | *25%* | 11-17 |



## Night
The following pokemon will spawn on Route 10 at night.

| Pokemon            | Image                                                                                                      | Area  | Rate  | Level |
| ------------------ | ---------------------------------------------------------------------------------------------------------- | ----- | ----- | ----- |
| **# 19** Rattata   | ![pokemonImage](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Sprites/19.png) | Grass | *15%* | 18    |
| **# 20** Raticate  | ![pokemonImage](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Sprites/20.png) | Grass | *5%*  | 20    |
| **# 29** Nidoran   | ![pokemonImage](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Sprites/29.png) | Grass | *10%* | 17    |
| **# 32** Nidoran   | ![pokemonImage](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Sprites/32.png) | Grass | *10%* | 17    |
| **# 66** Machop    | ![pokemonImage](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Sprites/66.png) | Grass | *5%*  | 16-18 |
| **# 81** Magnemite | ![pokemonImage](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Sprites/81.png) | Grass | *55%* | 16-22 |

## Fishing
The following pokemon will spawn on Route 10 by fishing.

| Pokemon            | Image                                                                                                       | Area      | Rate   | Level |
| ------------------ | ----------------------------------------------------------------------------------------------------------- | --------- | ------ | ----- |
| **# 129** Magikarp | ![pokemonImage](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Sprites/129.png) | Old Rod   | *100%* | 5     |
| **# 60** Poliwag   | ![pokemonImage](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Sprites/60.png)  | Good Rod  | *50%*  | 10    |
| **# 118** Goldeen  | ![pokemonImage](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Sprites/118.png) | Good Rod  | *50%*  | 10    |
| **# 61** Poliwhirl | ![pokemonImage](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Sprites/61.png)  | Super Rod | *25%*  | 23    |
| **# 79** Slowpoke  | ![pokemonImage](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Sprites/79.png)  | Super Rod | *25%*  | 15    |
| **# 98** Krabby    | ![pokemonImage](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Sprites/98.png)  | Super Rod | *20%*  | 15-20 |
| **# 99** Kingler   | ![pokemonImage](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Sprites/99.png)  | Super Rod | *10%*  | 25    |
| **# 116** Horsea   | ![pokemonImage](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Sprites/116.png) | Super Rod | *20%*  | 10    |

# NPCs
The following trainers are on Route 10.
## Engineer Lincoln
![trainerImage](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Trainer%20Sprites/Engineer1.png)

*Location: Route 9*

### Rewards
- $ 500
### Party
 | Party                                                                                                        | Pokemon           | Level | Special Moveset |
 | ------------------------------------------------------------------------------------------------------------ | ----------------- | ----- | --------------- |
 | ![pokemonParty1](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Sprites/100.png) | **# 100** Voltorb | 21    | Default         |
 | ![pokemonParty2](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Sprites/100.png) | **# 100** Voltorb | 21    | Default         |
 | ![pokemonParty3](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Sprites/100.png) | **# 100** Voltorb | 21    | Default         |

