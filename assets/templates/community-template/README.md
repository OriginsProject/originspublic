<center>

![Trainer](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/trainer.png)

</center>

<div align="right">

![Trainer](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/ORIGINS.png)


</div>

# Community Template
With the addition of mods and more community content/involvement, there needs to be a standard that we use when communicating information. The form of graphics not only present our ideas in a pleasing way, but also has a higher potential for people to read it or look at it.

- [Community Template](#community-template)
- [Fonts](#fonts)
  - [Headers](#headers)
  - [Text](#text)
- [Picture](#picture)
- [Creating Content](#creating-content)

# Fonts
The fonts included in this folder should be used for any text used in an Origins Community annoucement graphic.

- Estrangelo Edessa Regular.tff
- Kozuka Gothic Pro Bold.otf
- Microsoft Yi Baiti Regular.tff

Just downloading and opening up these files should give you the options to install them on your operating system as a font.

## Headers
Headers should use the `Microsoft Yi Baiti` font. Font color should be `#f2754f` and it should have a 1 pixel border with the color `#32130b` (Assuming you can apply a border to the text in your photo editor)

## Text
Anything that isn't a header should use the `Estangelo Edessa` font. Font color should be `#d3bdbd`.

# Picture
The template should use this picture:

![templateImage](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/templates/community-template/CommunityBackgroundTemplate.png)

# Creating Content

After installing the fonts and downloading the picture, add any text or grahpics to the image using any photo editor (Paint, GIMP, Photoshop, etc.) of your choice.


