## Credits
- Alex
- Origins 1.0 Team
- TaraSly
- DR Damian
- Jax
- Luber
- Miss Rona
- LwoJX
- Tsunayoshi
- Sabriina
- Lucas
- Ter13
- KaioChao
- Ashikaga
- Tsunayoshi

*A huge thank you to everyone who has worked on or contributed to Origins.*

<img src="/assets/images/thank-you.png"  width="35%">