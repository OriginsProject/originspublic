<div align="center">
<a href="https://discord.gg/wuJBzj2">
         <img alt="Discord" src="assets/images/origins-gitlab-3.png" width = "75%">
</a>
</div>

#

<div align="center">
<a href="https://discord.gg/wuJBzj2">
         <img alt="Discord" src="assets/images/origins-discord.png"
         width="100" height="100">
</a>
        <a href="http://www.byond.com/games/Hinashou/Origins">
         <img alt="BYOND" src="assets/images/byond-logo.png"
         width=100" height="100">
</a>
</div>

# Origins

Relive your most nostalgic moments from a vast region in this upcoming title. Origins is centered around the most iconic region and our favorite catchable monsters.

<img src="/assets/images/game-preview.gif"  width="50%">


# A New Twist
Our favorite handheld games go back to the 90's. However, there was always something I felt was missing, **action based multiplayer combat**.

<div align="right">
<img src="/assets/images/combat-preview.gif"  width="50%">
</div>


By being true to the Origins of our favorite games, this game looks to redefine a classic and bring it into the next era. **Your journey awaits you!**

# Unique Features
*  **Realtime Action-based combat**
*  **Online multiplayer**
*  **Dynamic Vegetation, Dynamic Shadows**

<img src="/assets/images/origins-dynamic-runtime-shadows-vegetation.png">

<img src="/assets/images/origins-cave-map-generate.png">

*Each time the world is ran, it dynamically generates shadows and vegetation in real time creating a unique world each time the server is updated or rebooted.*
*  **PvP & PvE**
*  **Player Gym Leaders**
*  **Full DevOps Process**

<img src="/assets/images/origins-ci-cd2.png">

*Code is managed in GitLab and has a dedicated build and deployment pipeline. This took quite a bit of time to set up, but I feel like it is super important for a seemless update process and a clean development environment.*

**Hinashou's CI/CD Pipeline for Origins**

```
Pipeline consists of 3 steps:
build
Pulls down an image from docker, and installs all necessary dependencies. Then, it runs through the code and ensures there are no compilation errors.
auto-deploy
Deploys the files generated from the build step to my remote game server. The remote game server detects these changes immediately, saves all players currently logged in, and restarts the game world with the updated files. It also sends out a discord message to my discord server with updated change logs.
manual-deploy
This step is also dependent on the build step, but is optional. It is only ran if the game is not currently running on the remote game server. After deploying the files to the remote game server, it will start the game world.

If the game world crashes for any reason on the remote game server, it will automatically restart within 1 minute.

Fully independent test game server and production game server with independent pipelines, but follow the same steps.
```


* **Discord Integration**

<img src="/assets/images/origins-discord-2.png">

<img src="/assets/images/Origins-discord-3.png">


*Get current server information, players online, lookup trainers, lookup pokemon, and generate pokemon at a specific level. Has the same setup as the game server, there is a test bot and a production bot.*

* **Online 24/7!**

# Next Major Feature:

*  **Expanded PVP System & Lore**


# Setup

1.  Visit the [**BYOND download page**](http://www.byond.com/download/) and download the latest version. The software is easy to install and best of all it's free. It allows you to launch **Origins**.

2.  Run the installer after download. After installation is complete, the BYOND program will automatically open.

3.  Create a BYOND account on the [**BYOND Registration Page**](https://secure.byond.com/Join/). This account is what you will use to log into the BYOND program.

*Note that your in-game name can be different from the name of your BYOND key.*

4.  After logging in, join by visiting [**Origins's BYOND Page**](http://www.byond.com/games/Hinashou/Origins) and hit **Play** to launch the game or search *Origins* in your BYOND Program to find us as well.

# Make Suggestions, Bug Reports & Issues

**Origins** is still in active development. The team relies on community feedback and reporting to further improve the game.

If you encounter any issues or bugs or have a suggestion, please report them on the [**Origins GitLab Issues Page**](https://gitlab.com/OriginsProject/originspublic/-/issues)

Please use the template provided when you go to make a new bug report or suggestion. To see what is currently proposed, accepted, and in-work, visit the [**Origins GitLab Development Board.**](https://gitlab.com/OriginsProject/originspublic/-/boards/1713984)

- Suggestion: ~Suggestion
- Bugs/Issues: ~Bug
- Accepted Items to be Worked: ~"Development::Accepted"
- Items currently in work: ~"Development::Doing"

# Contributing

Anyone who contributes to the development of Origins and their work is added in the game will receive the in-game role of **Contributor**

* Contributor login message
* Contributor title in OOC
* Contributor Discord role
* Ability to select 2 starters
* Name included in the [Origins Credits](Credits.md)
* +5% EXP gain

To contribute directly to this project repository [Request Access to OriginsPublic](https://gitlab.com/OriginsProject/originspublic/-/project_members/request_access)

# The Team

- Owner/Developer: Hinashou




