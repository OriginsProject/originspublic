# Origins Changelog
- [Origins Changelog](#origins-changelog)
  - [Latest](#latest)
  - [Version 2.86.3](#version-2863)
    - [Bug Fixes](#bug-fixes)
  - [Version 2.86.1](#version-2861)
    - [New Features](#new-features)
    - [Bug Fixes](#bug-fixes-1)
  - [Version 2.85.0](#version-2850)
    - [New Features](#new-features-1)
  - [Version 2.84.0](#version-2840)
    - [Bug Fixes](#bug-fixes-2)
  - [Version 2.83.0](#version-2830)
    - [New Features](#new-features-2)
  - [Version 2.81.2](#version-2812)
    - [New Features](#new-features-3)
    - [Bug Fixes](#bug-fixes-3)
  - [Version 2.80.3](#version-2803)
    - [New Features](#new-features-4)
    - [Bug Fixes](#bug-fixes-4)
  - [Version 2.77.4](#version-2774)
    - [New Features](#new-features-5)
    - [Bug Fixes](#bug-fixes-5)
  - [Version 2.75.3](#version-2753)
    - [New Features](#new-features-6)
    - [Bug Fixes](#bug-fixes-6)
  - [Version 2.74.0](#version-2740)
    - [New Features](#new-features-7)
  - [Version 2.72.1](#version-2721)
    - [New Features](#new-features-8)
    - [Bug Fixes](#bug-fixes-7)
  - [Version 2.70.0](#version-2700)
    - [New Features](#new-features-9)
  - [Version 2.65.2](#version-2652)
    - [New Features](#new-features-10)
    - [Bug Fixes](#bug-fixes-8)
    - [New Features](#new-features-11)
  - [Version 2.64.2](#version-2642)
    - [New Features](#new-features-12)
  - [Version 2.64.1](#version-2641)
    - [New Features](#new-features-13)
  - [Version 2.64.0](#version-2640)
    - [New Features](#new-features-14)
  - [Version 2.61.2](#version-2612)
    - [Bug Fixes](#bug-fixes-9)
  - [Version 2.61.1](#version-2611)
    - [Bug Fixes](#bug-fixes-10)
  - [Version 2.61.0](#version-2610)
    - [New Features](#new-features-15)
  - [Version 2.60.5](#version-2605)
    - [Bug Fixes](#bug-fixes-11)
  - [Version 2.60.4](#version-2604)
    - [New Features](#new-features-16)
    - [Bug Fixes](#bug-fixes-12)
  - [Version 2.58.3](#version-2583)
    - [New Features](#new-features-17)
    - [Bug Fixes](#bug-fixes-13)
  - [Version 2.52.5](#version-2525)
    - [New Features](#new-features-18)
    - [Bug Fixes](#bug-fixes-14)
  - [Version 2.48.0](#version-2480)
  - [Version 2.47.5](#version-2475)
    - [New Features](#new-features-19)
    - [Bug Fixes](#bug-fixes-15)
  - [Version 2.44.0](#version-2440)
    - [New Features](#new-features-20)
  - [Version 2.43.1](#version-2431)
    - [Bug Fixes](#bug-fixes-16)
  - [Version 2.43.0](#version-2430)
    - [New Features](#new-features-21)
  - [Version 2.40.0](#version-2400)
    - [New Features](#new-features-22)
  - [Version 2.38.2](#version-2382)
    - [New Features](#new-features-23)
    - [Bug Fixes](#bug-fixes-17)
  - [Version 2.31.4](#version-2314)
    - [Bug Fixes](#bug-fixes-18)
  - [Version 2.31.1](#version-2311)
    - [New Features](#new-features-24)
    - [Bug Fixes](#bug-fixes-19)
  - [Version 2.30.2](#version-2302)
    - [New Features](#new-features-25)
    - [Bug Fixes](#bug-fixes-20)
  - [Version 2.27.1](#version-2271)
    - [New Features](#new-features-26)
    - [Bug Fixes](#bug-fixes-21)
  - [Version 2.23.0](#version-2230)
    - [New Features](#new-features-27)
  - [Version 2.22.0](#version-2220)
    - [New Features](#new-features-28)
  - [Version 2.21.0](#version-2210)
    - [New Features](#new-features-29)
  - [Version 2.20.1](#version-2201)
    - [New Features](#new-features-30)
    - [Bug Fixes](#bug-fixes-22)
  - [Version 2.17.0](#version-2170)
    - [New Features](#new-features-31)
  - [Version 2.13.0](#version-2130)
    - [New Features](#new-features-32)
    - [Bug Fixes](#bug-fixes-23)
  - [Version 2.12.0](#version-2120)
    - [New Features](#new-features-33)
    - [Bug Fixes](#bug-fixes-24)
  - [Version 2.11.0](#version-2110)
    - [New Features](#new-features-34)
    - [Bug Fixes](#bug-fixes-25)
  - [Version 2.10.2](#version-2102)
    - [Bug Fixes](#bug-fixes-26)
  - [Version 2.10.0](#version-2100)
    - [New Features](#new-features-35)
  - [Version 2.09.12](#version-20912)
    - [New Features](#new-features-36)
    - [Bug Fixes](#bug-fixes-27)
  - [Version 2.08.5](#version-2085)
    - [Bug Fixes](#bug-fixes-28)
  - [Version 2.08.4](#version-2084)
    - [New Features](#new-features-37)
    - [Bug Fixes](#bug-fixes-29)
  - [Version 2.07.4](#version-2074)
    - [New Features](#new-features-38)
    - [Bug Fixes](#bug-fixes-30)
  - [Version 2.05.1](#version-2051)
    - [New Features](#new-features-39)
    - [Bug Fixes](#bug-fixes-31)
  - [Version 2.03.1](#version-2031)
    - [New Features](#new-features-40)
    - [Bug Fixes](#bug-fixes-32)
  - [Version 2.02.7](#version-2027)
    - [Bug Fixes](#bug-fixes-33)
  - [Version 2.02.3](#version-2023)
    - [Bug Fixes](#bug-fixes-34)
  - [Version 2.02.1](#version-2021)
    - [Milestones](#milestones)
    - [New Features](#new-features-41)
    - [Bug Fixes](#bug-fixes-35)
  - [Version 1.98.0](#version-1980)
    - [New Features](#new-features-42)
  - [Version 1.97.1](#version-1971)
    - [Milestones](#milestones-1)
    - [New Features](#new-features-43)
    - [Bug Fixes](#bug-fixes-36)
  - [Version 1.86.2](#version-1862)
    - [New Features](#new-features-44)
    - [Bug Fixes](#bug-fixes-37)
  - [Version 1.82.2](#version-1822)
    - [New Features](#new-features-45)
    - [Bug Fixes](#bug-fixes-38)
  - [Version 1.80.0](#version-1800)
    - [New Features](#new-features-46)
  - [Version 1.79.4](#version-1794)
    - [Bug Fixes](#bug-fixes-39)
  - [Version 1.79.2](#version-1792)
    - [New Features](#new-features-47)
    - [Bug Fixes](#bug-fixes-40)
  - [Version 1.74.1](#version-1741)
    - [New Features](#new-features-48)
    - [Bug Fixes](#bug-fixes-41)
  - [Version 1.72.1](#version-1721)
    - [New Features](#new-features-49)
    - [Bug Fixes](#bug-fixes-42)
  - [Version 1.70.3](#version-1703)
    - [Bug Fixes](#bug-fixes-43)
    - [Other](#other)
  - [Version 1.70.1](#version-1701)
    - [New Features:](#new-features-50)
    - [Bug Fixes:](#bug-fixes-44)
  - [Version 1.69.1](#version-1691)
    - [New Features:](#new-features-51)
    - [Bug Fixes:](#bug-fixes-45)
  - [Version 1.68.4](#version-1684)
    - [Bug Fixes:](#bug-fixes-46)
  - [Version 1.68.3](#version-1683)
    - [Bug Fixes:](#bug-fixes-47)
  - [Version 1.68.2](#version-1682)
    - [New Features:](#new-features-52)
    - [Bug Fixes:](#bug-fixes-48)
  - [Version 1.59.5](#version-1595)
    - [Bug Fixes:](#bug-fixes-49)
  - [Version 1.59.3](#version-1593)
    - [Bug Fixes:](#bug-fixes-50)
  - [Version 1.59.2](#version-1592)
    - [Bug Fixes:](#bug-fixes-51)
  - [Version 1.59.0](#version-1590)
    - [New Features:](#new-features-53)
    - [Bug Fixes:](#bug-fixes-52)
  - [Version 1.58.1](#version-1581)
    - [Bug Fixes](#bug-fixes-53)
  - [Version 1.58.0](#version-1580)
    - [New Features](#new-features-54)
  - [Version 1.55.0](#version-1550)
    - [New Features](#new-features-55)
  - [Version 1.54.8](#version-1548)
    - [Bug Fixes](#bug-fixes-54)
  - [Version 1.54.0](#version-1540)
    - [New Features:](#new-features-56)
    - [Bug Fixes](#bug-fixes-55)
  - [Version 1.53.0](#version-1530)
    - [New Features:](#new-features-57)
    - [Bug Fixes](#bug-fixes-56)
  - [Version 1.52.1](#version-1521)
    - [Bug Fixes:](#bug-fixes-57)
  - [Version 1.52.0](#version-1520)
    - [New Features:](#new-features-58)
    - [Bug Fixes](#bug-fixes-58)
  - [Version 1.51.1](#version-1511)
    - [Bug Fixes](#bug-fixes-59)
  - [Version 1.51.0](#version-1510)
    - [New Features:](#new-features-59)
    - [Bug Fixes:](#bug-fixes-60)
  - [Version 1.50.3](#version-1503)
    - [Bug Fixes:](#bug-fixes-61)
  - [Version 1.50](#version-150)
    - [New Features:](#new-features-60)
    - [Bug Fixes:](#bug-fixes-62)
    - [Enhancements:](#enhancements)
  - [Version 1.10](#version-110)
## Latest
## Version 2.86.3
### Bug Fixes
* [Trading Machine](https://gitlab.com/OriginsProject/originspublic/-/issues/424)
* [Scroll Text Fix](https://gitlab.com/OriginsProject/originspublic/-/issues/425)
## Version 2.86.1
### New Features
* [Text dialogue - space to continue](https://gitlab.com/OriginsProject/originspublic/-/issues/423)
### Bug Fixes
* [Move Relearner](https://gitlab.com/OriginsProject/originspublic/-/issues/422)
## Version 2.85.0
### New Features
* [Automated Game Events](https://gitlab.com/OriginsProject/originspublic/-/issues/421)
## Version 2.84.0
### Bug Fixes
* [Fix hitshake animation](https://gitlab.com/OriginsProject/originspublic/-/issues/420)
## Version 2.83.0
### New Features
* [Events Persist Through Reboot](https://gitlab.com/OriginsProject/originspublic/-/issues/402)
* [OOC and Say Default Macros](https://gitlab.com/OriginsProject/originspublic/-/issues/403)
## Version 2.81.2
### New Features
* [Pokeball Sounds](https://gitlab.com/OriginsProject/originspublic/-/issues/401)
* [Make Money Text Smaller](https://gitlab.com/OriginsProject/originspublic/-/issues/399)

### Bug Fixes
* [Animate Bug](https://gitlab.com/OriginsProject/originspublic/-/issues/398)
* [Base Icon Losing Color on Relog](https://gitlab.com/OriginsProject/originspublic/-/issues/400)

## Version 2.80.3
### New Features
* [Lower World FPS](https://gitlab.com/OriginsProject/originspublic/-/issues/394)
* [Update BYOND version to 514.1566](https://gitlab.com/OriginsProject/originspublic/-/issues/395)
* [Revert To Original Inputs](https://gitlab.com/OriginsProject/originspublic/-/issues/396)

### Bug Fixes
* [Location Tile Lag](https://gitlab.com/OriginsProject/originspublic/-/issues/391)
* [Hydro Pump and Hyper Beam Reset Shiny Effect](https://gitlab.com/OriginsProject/originspublic/-/issues/393)
* [Tutorial Bugged](https://gitlab.com/OriginsProject/originspublic/-/issues/397)

## Version 2.77.4

### New Features
* [Update Party Icons](https://gitlab.com/OriginsProject/originspublic/-/issues/388)
* [Remove Icon Data In Saves](https://gitlab.com/OriginsProject/originspublic/-/issues/389)

### Bug Fixes
* [Logging Out Taking Too Long](https://gitlab.com/OriginsProject/originspublic/-/issues/340)
* [Last Pokeball When Catching](https://gitlab.com/OriginsProject/originspublic/-/issues/379)
* [Water Forest Battlefield Movement](https://gitlab.com/OriginsProject/originspublic/-/issues/385)
* [Egg in Slot 1 Doesn't Trigger Damage Sendouts](https://gitlab.com/OriginsProject/originspublic/-/issues/387)
## Version 2.75.3
### New Features

* [Rework Battle Map Generation System](https://gitlab.com/OriginsProject/originspublic/-/issues/382)

### Bug Fixes
* [Slowpoke -> Slowking Evolution](https://gitlab.com/OriginsProject/originspublic/-/issues/378)
* [Skitty Can't Evolve](https://gitlab.com/OriginsProject/originspublic/-/issues/383)
* [Barboach Can't Evolve](https://gitlab.com/OriginsProject/originspublic/-/issues/384)

## Version 2.74.0

### New Features
* [Sky Attack Range Buff](https://gitlab.com/OriginsProject/originspublic/-/issues/377)
* [Mapping Environment Suggestions from Tsuna and Ashi](https://gitlab.com/OriginsProject/originspublic/-/issues/376)
## Version 2.72.1
### New Features
- [Allow Players to Select Battle Map](https://gitlab.com/OriginsProject/originspublic/-/issues/373)

### Bug Fixes
- [Technician Ability](https://gitlab.com/OriginsProject/originspublic/-/issues/365)
- [Dig Bugged](https://gitlab.com/OriginsProject/originspublic/-/issues/374)

## Version 2.70.0
### New Features
- [Sky Attack Move Rework](https://gitlab.com/OriginsProject/originspublic/-/issues/368)
- [Rework Nameplate System](https://gitlab.com/OriginsProject/originspublic/-/issues/369)
- [Rework Damage Effect](https://gitlab.com/OriginsProject/originspublic/-/issues/370)
- [Max Damage](https://gitlab.com/OriginsProject/originspublic/-/issues/371)
- [Rework Pokemon Meters](https://gitlab.com/OriginsProject/originspublic/-/issues/372)
## Version 2.65.2

### New Features
- [Give Moderators the Ability to Host Community Events](https://gitlab.com/OriginsProject/originspublic/-/issues/364)

### Bug Fixes
- [LearnMoves Get Deleted](https://gitlab.com/OriginsProject/originspublic/-/issues/363)
- [Surf bugged in relearner](https://gitlab.com/OriginsProject/originspublic/-/issues/362). Still isn't added as a programmed move, but shouldn't give this error anymore.

### New Features
- [Ability for Mods to Run Events](https://gitlab.com/OriginsProject/originspublic/-/issues/361)

## Version 2.64.2

### New Features
- [Add Tsunayoshi and Ashikaga as Mods in the game](https://gitlab.com/OriginsProject/originspublic/-/issues/361)

## Version 2.64.1

### New Features
- [Move Relearner Move Source](https://gitlab.com/OriginsProject/originspublic/-/issues/360)
- [Update TMs](https://gitlab.com/OriginsProject/originspublic/-/issues/359)
  - [TMs Wiki](https://gitlab.com/OriginsProject/originspublic/-/wikis/TMs)

Older version of the TMs file was used in the last update. Should be correct now!

## Version 2.64.0
### New Features
- [Update Learn Moves](https://gitlab.com/OriginsProject/originspublic/-/issues/357)
  - [Learn Moves Wiki](https://gitlab.com/OriginsProject/originspublic/-/wikis/Learn-Moves)
- [Update Egg Moves](https://gitlab.com/OriginsProject/originspublic/-/issues/358) 
  - [Egg Moves Wiki](https://gitlab.com/OriginsProject/originspublic/-/wikis/Egg-Moves)
- [Update TMs](https://gitlab.com/OriginsProject/originspublic/-/issues/359)
  - [TMs Wiki](https://gitlab.com/OriginsProject/originspublic/-/wikis/TMs)

Huge thanks to **Tsunayoshi** for compiling the data for this update! 
## Version 2.61.2
### Bug Fixes
- [Burn proc Infinite Loop](https://gitlab.com/OriginsProject/originspublic/-/issues/355). Oh boy was this fix a long time coming. Since what feels like the dawn of time, there has been an issue that causes the server to lock up and freeze. This was at least one issue that caused this. Hopefully there aren't others, but glad to finally get this one behind us.
## Version 2.61.1
### Bug Fixes
- [Various runtime errors and defeating wild monsters triggering pokedex lag](https://gitlab.com/OriginsProject/originspublic/-/issues/355)
## Version 2.61.0
### New Features
- [Rollback Game Engine to latest stable version](https://gitlab.com/OriginsProject/originspublic/-/issues/354)

## Version 2.60.5

### Bug Fixes

- [Small breakfix when the player logs out and the pokemon they have may still persist.](https://gitlab.com/OriginsProject/originspublic/-/issues/352)

## Version 2.60.4

### New Features
* [Performance Improvements For the Dex](https://gitlab.com/OriginsProject/originspublic/-/issues/346)
* [Tick Usage General Proc](https://gitlab.com/OriginsProject/originspublic/-/issues/345)
* [Give Egg to Player](https://gitlab.com/OriginsProject/originspublic/-/issues/342)

### Bug Fixes
* [Move Pokemon to Full Box](https://gitlab.com/OriginsProject/originspublic/-/issues/339)
* [Ability Check Runtime Error](https://gitlab.com/OriginsProject/originspublic/-/issues/341)
* [Fireblast Runtime Error](https://gitlab.com/OriginsProject/originspublic/-/issues/343)
* [Other Runtime Errors](https://gitlab.com/OriginsProject/originspublic/-/issues/344)
## Version 2.58.3
### New Features
* [Space to Interact](https://gitlab.com/OriginsProject/originspublic/-/issues/225): Players can now use their space key to talk to NPCs or interact with objects in the world.
* [Hotkeys for sending out Pokemon](https://gitlab.com/OriginsProject/originspublic/-/issues/225): By using `Ctrl+1`, `Ctrl+2`, etc. the player is now able to quickly send out or return their pokemon back to their party.
* [Pokemon Cries](https://gitlab.com/OriginsProject/originspublic/-/issues/334): Not only did we add hundreds of missing pokemon cries, all the existing cries have been updated as well.
* [Non-Overworld Sprite Update](https://gitlab.com/OriginsProject/originspublic/-/issues/335): Using the latest sprites for the Pokemon window and Pokedex image.
* [Pokedex Information for All Pokemon](https://gitlab.com/OriginsProject/originspublic/-/issues/336): Add Pokedex descriptions for all the pokemon in the game.
* [Send out next avaialble Pokemon when attacked](https://gitlab.com/OriginsProject/originspublic/-/issues/337): The wild is now much more dangerous, as pokemon attacking you will draw you out into battle. This will play nicely into future plans for PvP where battling can happen in the open world.

### Bug Fixes
* [Cave Map Tile Error](https://gitlab.com/OriginsProject/originspublic/-/issues/332)
* [Background HUD UI misalignment](https://gitlab.com/OriginsProject/originspublic/-/issues/333)
* [Various null pointers](https://gitlab.com/OriginsProject/originspublic/-/issues/338)

## Version 2.52.5
### New Features
* [Shedinja!](https://gitlab.com/OriginsProject/originspublic/-/issues/319)
* [Catch Quest Route 1](https://gitlab.com/OriginsProject/originspublic/-/issues/320)
* [Yawn Sleep Message](https://gitlab.com/OriginsProject/originspublic/-/issues/321)
* [Force Tile Movement Mode](https://gitlab.com/OriginsProject/originspublic/-/issues/328)
* [Improve Performance with Flying Flocks](https://gitlab.com/OriginsProject/originspublic/-/issues/330)
### Bug Fixes
* [Pokemon cannot move after being caught](https://gitlab.com/OriginsProject/originspublic/-/issues/87)
* [Can't change savings %](https://gitlab.com/OriginsProject/originspublic/-/issues/322)
* [Runtime Error: Bump](https://gitlab.com/OriginsProject/originspublic/-/issues/329)
* [Level Up Sound Not Following Sound Effect Volume](https://gitlab.com/OriginsProject/originspublic/-/issues/331)

## Version 2.48.0

* [Update BYOND version to 5.14.1560](https://gitlab.com/OriginsProject/originspublic/-/issues/327)
## Version 2.47.5

### New Features
* [Catch Minigame Mechanic](https://gitlab.com/OriginsProject/originspublic/-/issues/278)
* [Catch Quest Route 1](https://gitlab.com/OriginsProject/originspublic/-/issues/280)
* [Create a Quest Giver NPC Template](https://gitlab.com/OriginsProject/originspublic/-/issues/281)
### Bug Fixes
* [Wurmple Not Evolving](https://gitlab.com/OriginsProject/originspublic/-/issues/253)
* [Aron/Lairon/Aggron Cant Learn Earthquake](https://gitlab.com/OriginsProject/originspublic/-/issues/282)
* [NotMob ScrollText Not Show ScrollingText](https://gitlab.com/OriginsProject/originspublic/-/issues/289)
* [Pokedex Button Shows on Load Without Quest](https://gitlab.com/OriginsProject/originspublic/-/issues/312)
* [Pokedex Item Doesnt Disappear](https://gitlab.com/OriginsProject/originspublic/-/issues/314)

This was a really fun update to work on, and proved to be challenging as well with refactoring legacy quests to match the new Quest templates. Congrats to Amaneur for becoming a contributor!


## Version 2.44.0

### New Features
* [Save Map Player Zoom Setting](https://gitlab.com/OriginsProject/originspublic/-/issues/252)


## Version 2.43.1

### Bug Fixes
* [Level Up Bug](https://gitlab.com/OriginsProject/originspublic/-/issues/251)

Special shoutout to Yuiz, Sabrina, and others for reporting this bug.

## Version 2.43.0

### New Features
* [Dark Theme UI](https://gitlab.com/OriginsProject/originspublic/-/issues/248)
* [Reorganize skin elements](https://gitlab.com/OriginsProject/originspublic/-/issues/249)
* [Chat Display Settings](https://gitlab.com/OriginsProject/originspublic/-/issues/250)

This update was a much needed overhaul of the current skin elements in the game. A lot of these components will be replaced with the new on screen UI objects, but this sets up a solid foundation for the transition and gives a lot more real estate to the player for the game screen.

## Version 2.40.0

### New Features
* [Change Default Game Map Ratio To Stretch To Fit](https://gitlab.com/OriginsProject/originspublic/-/issues/246)
* [Update Chat Text](https://gitlab.com/OriginsProject/originspublic/-/issues/247)

## Version 2.38.2

### New Features
* [Location Title Panel](https://gitlab.com/OriginsProject/originspublic/-/issues/234)
* [Scrollin Text Panel](https://gitlab.com/OriginsProject/originspublic/-/issues/238)
* [Quests UI](https://gitlab.com/OriginsProject/originspublic/-/issues/239)
* [Move Hotkeys UI](https://gitlab.com/OriginsProject/originspublic/-/issues/240)
* [Move Cooldowns UI](https://gitlab.com/OriginsProject/originspublic/-/issues/241)
* [Quest Button Pokedex Button UI](https://gitlab.com/OriginsProject/originspublic/-/issues/242)
* [Game Window UI](https://gitlab.com/OriginsProject/originspublic/-/issues/243)
### Bug Fixes
* [Runetime Error: StatExp](https://gitlab.com/OriginsProject/originspublic/-/issues/244)
* [Update VSCode Debugger Tool Integration](https://gitlab.com/OriginsProject/originspublic/-/issues/245)

## Version 2.31.4

### Bug Fixes
* [Electric type moves super effective against rock types](https://gitlab.com/OriginsProject/originspublic/-/issues/235)
* [Battle AI Roar Causes Battle Stalemate](https://gitlab.com/OriginsProject/originspublic/-/issues/236)
* [Links in Right Panel Broken](https://gitlab.com/OriginsProject/originspublic/-/issues/237)


## Version 2.31.1

### New Features
* [Update BYOND version to 513.1542](https://gitlab.com/OriginsProject/originspublic/-/issues/232)


### Bug Fixes
* [Player Logout Cleanup](https://gitlab.com/OriginsProject/originspublic/-/issues/231)


## Version 2.30.2

### New Features
* [Reboot Cycle Event](https://gitlab.com/OriginsProject/originspublic/-/issues/229). The world will now update every 12 hours.
* [Mapper Environment Suggestions](https://gitlab.com/OriginsProject/originspublic/-/issues/216)
* [Definitions File](https://gitlab.com/OriginsProject/originspublic/-/issues/226)


### Bug Fixes
* [Status Move AI Behavior](https://gitlab.com/OriginsProject/originspublic/-/issues/227). Fixed some weird AI behavior when using certain moves like Sword Dance.
* [Rogue Player Reference](https://gitlab.com/OriginsProject/originspublic/-/issues/228). Fix to a potential BYOND reference issue. Keeping an eye on this one to see if it comes up at a later date or fixed with a BYOND patch.


## Version 2.27.1

### New Features
* [Streamline Move AI Code Type 2](https://gitlab.com/OriginsProject/originspublic/-/issues/218)
* [Update World Status with Player Count](https://gitlab.com/OriginsProject/originspublic/-/issues/219)
* [Rollout Rework](https://gitlab.com/OriginsProject/originspublic/-/issues/220)

**Description**:

- **Type**: Rock
- **Power**: 20
- **PP**: 20
- **MPP**: 20
- **Cooldown**: 13 seconds

Pokemon will begin moving in a straight line. User can change directions. The more time increases the faster the Pokemon will go and the power will increase to a max of 3x base power. If the Pokemon runs into anything, it will hit with Rollout and stop the move. The damage from Rollout is doubled for 10 seconds if Defense Curl was activated 10 seconds ago or less. Rollout lasts for 5 seconds total.

![Rollout](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Moves/Rollout.gif)

* [Reduce Solar Beam Firing Time](https://gitlab.com/OriginsProject/originspublic/-/issues/222)

### Bug Fixes
* [Solar Beam Icon Missing](https://gitlab.com/OriginsProject/originspublic/-/issues/221)

## Version 2.23.0

### New Features
* [Streamline Move AI Code](https://gitlab.com/OriginsProject/originspublic/-/issues/217)

## Version 2.22.0

### New Features
* [Rock Slide Rework](https://gitlab.com/OriginsProject/originspublic/-/issues/215)

![RockSlide](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Moves/RockSlide.gif)

## Version 2.21.0

### New Features
* [Cotton Spore Move](https://gitlab.com/OriginsProject/originspublic/-/issues/181)

![CottonSpore](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Moves/CottonSpore.gif)

## Version 2.20.1

### New Features
* [Display TempStats to Battle Log](https://gitlab.com/OriginsProject/originspublic/-/issues/212)
* [Cosmic Power Move](https://gitlab.com/OriginsProject/originspublic/-/issues/213)

![CosmicPower](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Moves/CosmicPower.gif)


* [Fire Spin Move](https://gitlab.com/OriginsProject/originspublic/-/issues/214)

![FireSpin](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Moves/FireSpin.gif)


### Bug Fixes
*[AI Pokemon Double Slap](https://gitlab.com/OriginsProject/originspublic/-/issues/206)

## Version 2.17.0

### New Features
* [Ability to Play Sound Effects When Moves Are Used](https://gitlab.com/OriginsProject/originspublic/-/issues/207)
* [Thunder Visual and Audio Enhancement](https://gitlab.com/OriginsProject/originspublic/-/issues/208)

![Thunder](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Moves/Thunder.gif)

* [Toxic Rework](https://gitlab.com/OriginsProject/originspublic/-/issues/209)

![Toxic](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Moves/Toxic.gif)

* [Thunderbolt Sound](https://gitlab.com/OriginsProject/originspublic/-/issues/211)

![Thunderbolt](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Moves/Thunderbolt.gif)

* [New Move: Sand Tomb!](https://gitlab.com/OriginsProject/originspublic/-/issues/210)

![SandTomb](https://gitlab.com/OriginsProject/originspublic/-/raw/main/assets/images/Moves/SandTomb.gif)



## Version 2.13.0

### New Features
* [AI Pokemon Sing](https://gitlab.com/OriginsProject/originspublic/-/issues/205)

### Bug Fixes
* [Detect Move](https://gitlab.com/OriginsProject/originspublic/-/issues/183)

## Version 2.12.0

### New Features
* [Mapper Environment Objects](https://gitlab.com/OriginsProject/originspublic/-/issues/202)
* [Add Ashikaga as Origins Contributor](https://gitlab.com/OriginsProject/originspublic/-/issues/203)
* [Shadow Generation for All Indoor Objects](https://gitlab.com/OriginsProject/originspublic/-/issues/204)

### Bug Fixes
* [Sludge Debug Messages](https://gitlab.com/OriginsProject/originspublic/-/issues/201)

## Version 2.11.0

### New Features
* [Buttery Smooth: I Can't Believe Its Origins!](https://gitlab.com/OriginsProject/originspublic/-/issues/199)

### Bug Fixes
* [Wild Pokemon Not Battling](https://gitlab.com/OriginsProject/originspublic/-/issues/200)

## Version 2.10.2

### Bug Fixes

* [Battle Map Using Wrong Z Level for Template](https://gitlab.com/OriginsProject/originspublic/-/issues/197)
* [Ember Exception](https://gitlab.com/OriginsProject/originspublic/-/issues/198)

## Version 2.10.0

### New Features

* [Origins Mapping Environment](https://gitlab.com/OriginsProject/originspublic/-/issues/196). Huge update here with a lot of potential. Anyone can no help map for Origins! To download the files, please visit the [mapping-environment branch of this GitLab Repository](https://gitlab.com/OriginsProject/originspublic/-/tree/mapping-environment). This mapping environment is now built-in to the Origins pipeline so anytime there is an update, it automatically generates the latest version of the map!

## Version 2.09.12

### New Features
* [No EXP Item](https://gitlab.com/OriginsProject/originspublic/-/issues/195). Available in all Marts!

### Bug Fixes
* [ShinySparkle Overlay](https://gitlab.com/OriginsProject/originspublic/-/issues/194)
* [Mouse Opacity Water Overlays](https://gitlab.com/OriginsProject/originspublic/-/issues/193)
* [Muk Icon Stuck in Tackle State when Idle](https://gitlab.com/OriginsProject/originspublic/-/issues/192)
* [Sludge Stepped on Exceptions](https://gitlab.com/OriginsProject/originspublic/-/issues/191)
* [Attack Proc Exceptions](https://gitlab.com/OriginsProject/originspublic/-/issues/190)
* [Ice Beam Exception](https://gitlab.com/OriginsProject/originspublic/-/issues/189)
* [Auto Battle Exception](https://gitlab.com/OriginsProject/originspublic/-/issues/188)
* [Invalid Attack Macros](https://gitlab.com/OriginsProject/originspublic/-/issues/187)
* [Error on Pokemon Creation if List of No Abilities to Choose From](https://gitlab.com/OriginsProject/originspublic/-/issues/186)
* [No EXP for Pokemon who use Stat Effect Moves](https://gitlab.com/OriginsProject/originspublic/-/issues/185)
* [Not all Pokemon Who Battle get EXP](https://gitlab.com/OriginsProject/originspublic/-/issues/179)
* [Ice Beam Hits Multiple Times](https://gitlab.com/OriginsProject/originspublic/-/issues/128)


## Version 2.08.5

### Bug Fixes

* [Mob Not Logging in Correctly](https://gitlab.com/OriginsProject/originspublic/-/issues/184). Special thanks to **Bloggers** for being a guinea pig and unfortunate victim of this and bringing it to my attention.

## Version 2.08.4

### New Features
* [Update TM List](https://gitlab.com/OriginsProject/originspublic/-/issues/175). Special thanks to **LwoJX** for gathering the data for this!

### Bug Fixes
* [Milotic Icon Not Centered](https://gitlab.com/OriginsProject/originspublic/-/issues/173)
* [Onix Shadows](https://gitlab.com/OriginsProject/originspublic/-/issues/176)
* [Gyarados Shadows](https://gitlab.com/OriginsProject/originspublic/-/issues/177)
* [Lapras Shadows](https://gitlab.com/OriginsProject/originspublic/-/issues/178)

## Version 2.07.4

### New Features

* [Add Evolution Items to All Marts](https://gitlab.com/OriginsProject/originspublic/-/issues/168)
* [Add Message When Buying Items from the Shop](https://gitlab.com/OriginsProject/originspublic/-/issues/172)

### Bug Fixes

* [Golduck Icon Stuck in Tackle State](https://gitlab.com/OriginsProject/originspublic/-/issues/167)
* [Gastly GHOSTIN](https://gitlab.com/OriginsProject/originspublic/-/issues/169)
* [Phanpy's Fainted State Not Showing](https://gitlab.com/OriginsProject/originspublic/-/issues/170)
* [Water Pokemon Spawns Incorrectly Giving Shiny Message](https://gitlab.com/OriginsProject/originspublic/-/issues/171)

## Version 2.05.1
### New Features
* [Gen 2 & Gen 3 Pokemon Added!](https://gitlab.com/OriginsProject/originspublic/-/issues/164). This update has been frequently requested and wanted for some time now. Please visit the [Origins GitLab Wiki](https://gitlab.com/OriginsProject/originspublic/-/wikis/) to view all the details on specific location spawns that are currently in the game. Pokemon spawn availability will roughly follow what is available in Gold/Silver/Crystal & Ruby/Sapphire/Emerald based on the areas relative to Gyms (Example: Routes before the 1st Gym will have Pokemon from all the games before their own first Gyms). This will effectively add an additional 235 Pokemon to the Origins roster, and we're excited as to what this means for the community. Celebrate and come battle, train, and catch some of your favorite Pokemon! 
* [All Moves updated to Gen 3 sets](https://gitlab.com/OriginsProject/originspublic/-/issues/166)

### Bug Fixes
* [Day/Night Cycle in Viridian Forest](https://gitlab.com/OriginsProject/originspublic/-/issues/165)

## Version 2.03.1

### New Features

* [Update TMs to Gen 3 Set](https://gitlab.com/OriginsProject/originspublic/-/issues/161)

* [New TMs List Wiki](https://gitlab.com/OriginsProject/originspublic/-/wikis/TMs)

### Bug Fixes

* [Ember Not Hitting](https://gitlab.com/OriginsProject/originspublic/-/issues/162)

## Version 2.02.7

### Bug Fixes
* [Ember Blocking Paths](https://gitlab.com/OriginsProject/originspublic/-/issues/136)
* [Change Pokemon Move Order Command](https://gitlab.com/OriginsProject/originspublic/-/issues/159)
* [Machamp Icon](https://gitlab.com/OriginsProject/originspublic/-/issues/160)
* [Debug Messages on Login for Money Events](https://gitlab.com/OriginsProject/originspublic/-/issues/158)

## Version 2.02.3

### Bug Fixes
* [Shiny Notifications](https://gitlab.com/OriginsProject/originspublic/-/issues/157)
* [Admins Should Have Moderator Verbs](https://gitlab.com/OriginsProject/originspublic/-/issues/156)

## Version 2.02.1

### Milestones
* [Shiny Pokemon Update](https://gitlab.com/OriginsProject/originspublic/-/milestones/7)

The Shiny Pokemon update is here! This system is a little different than other shiny systems, as there are potentially endless possibilities of color combinations for pokemon. Some color changes will be subtle, while others will be more drastic. Happy shiny hunting :)!

### New Features
* [Shiny Pokemon](https://gitlab.com/OriginsProject/originspublic/-/issues/150)
* [Shiny Events](https://gitlab.com/OriginsProject/originspublic/-/issues/153)
* [Shiny Discord Notifications](https://gitlab.com/OriginsProject/originspublic/-/issues/154)

### Bug Fixes
* [Safari Zone Alliance Access](https://gitlab.com/OriginsProject/originspublic/-/issues/155)

## Version 1.98.0

### New Features
* [Template for Event Class](https://gitlab.com/OriginsProject/originspublic/-/issues/152)

This is mainly a backend feature that players won't notice. Just makes it easier to handle and develop new events easier in the future.


## Version 1.97.1

### Milestones
* [War Comes to Kanto](https://gitlab.com/OriginsProject/originspublic/-/milestones/6)

Most of the new features for this update are related to the new **War Comes to Kanto** quest! This is the introduction quest to the [Origins PvP and Lore](https://gitlab.com/OriginsProject/originspublic/-/issues/27) that is coming to Origins! 

To start this quest:

* Log in to Origins and have the Boulder Badge.

*or*

* Enter Pewter City

Hop in now and claim your benefits for joining an **Alliance!**

### New Features
* [Alliances Quest Log](https://gitlab.com/OriginsProject/originspublic/-/issues/127)
* [Add Parameters to ScrollingText proc](https://gitlab.com/OriginsProject/originspublic/-/issues/140)
* [War Comes to Kanto - Talk with Mike](https://gitlab.com/OriginsProject/originspublic/-/issues/142)
* [Alliance Changes should output to Discord](https://gitlab.com/OriginsProject/originspublic/-/issues/148)
* [Player Safari Zones](https://gitlab.com/OriginsProject/originspublic/-/issues/147)
* [War Comes to Kanto - Professor Oak](https://gitlab.com/OriginsProject/originspublic/-/issues/146)
* [War Comes to Kanto - Misty](https://gitlab.com/OriginsProject/originspublic/-/issues/145)
* [War Comes to Kanto - Brock](https://gitlab.com/OriginsProject/originspublic/-/issues/144)
* [War Comes to Kanto - Talk with Dorothy](https://gitlab.com/OriginsProject/originspublic/-/issues/143)
* [War Comes to Kanto - Call With Mom](https://gitlab.com/OriginsProject/originspublic/-/issues/141)
* [Quest Notification Sound Effect](https://gitlab.com/OriginsProject/originspublic/-/issues/139)

### Bug Fixes
* [Nameplates Not Showing](https://gitlab.com/OriginsProject/originspublic/-/issues/138)

## Version 1.86.2

### New Features
* [Stat EXP Events](https://gitlab.com/OriginsProject/originspublic/-/issues/132) 
* [Change EXP Bonus Message](https://gitlab.com/OriginsProject/originspublic/-/issues/131)
* [Allow Players to see Stat Exp](https://gitlab.com/OriginsProject/originspublic/-/issues/130)

  This information is now provided when you click your pokemon (just like checking any of your other stats.) You also have the ability to toggle stat EXP messages in the combat log by using the **Toggle Stat Notifications** in Settings.
* [Alliance Passives](https://gitlab.com/OriginsProject/originspublic/-/issues/126)


### Bug Fixes
* [Debug Messages from NPC Battles](https://gitlab.com/OriginsProject/originspublic/-/issues/134)
* [VSCode Dream Maker Client Extension Problems and Warnings](https://gitlab.com/OriginsProject/originspublic/-/issues/133)

## Version 1.82.2

### New Features
* [New Sound System](https://gitlab.com/OriginsProject/originspublic/-/issues/121). 
  
  This one has been in the backlog for quite some time. Players now have the ability to control the volume of music and sound effects. These settings can be found in the `Settings` tab. Range: **0-100** *(Default 5)*
NO MORE BLARING ORIGINS THEME SONG!
* [Let Players Change Pokemon Nicknames](https://gitlab.com/OriginsProject/originspublic/-/issues/124). 
  
  Command is located in the `Pokemon` Tab. Thanks **Lyric Chronos** for the great suggestion!
  
* [Cleanup Verbs](https://gitlab.com/OriginsProject/originspublic/-/issues/125)

  Removed some unused verbs and reorganized them in new tabs.

### Bug Fixes
* [Using Tackle Against A Pokemon Resets Speed](https://gitlab.com/OriginsProject/originspublic/-/issues/121). Overall, speed is now more consistent in setting and resetting from using moves like Tackle, Quick Attack, etc.) Professor Oak's Pikachu will no longer zoom away if you hit him with a tackle!
* [Screen Shake Effect Not Working in Battles](https://gitlab.com/OriginsProject/originspublic/-/issues/121) Oh boy, was this one an annoying bug to fix. I quickly noticed that this applied to most things in battle, but it only seemed to happen in battles involving NPCs. It led me down various troubleshooting rabbit holes and wasted a lot of time unfortunately, but I eventually figured out what the problem was. Thanks to **WolvesWithPie** for helping me diagnose the issue on the test server!

## Version 1.80.0

### New Features
* [Discord OOC Integration](https://gitlab.com/OriginsProject/originspublic/-/issues/107) Chat between players in game and on discord!
  

## Version 1.79.4

### Bug Fixes
* [Wing Attack Hits More Than Once](https://gitlab.com/OriginsProject/originspublic/-/issues/120)
* [Steel Wing Hots More Than Once](https://gitlab.com/OriginsProject/originspublic/-/issues/119)

**Shoutout to Alex for his first update going to the main server!**

## Version 1.79.2

### New Features
* [Improve Pokeball Return and Throw Animations](https://gitlab.com/OriginsProject/originspublic/-/issues/118)
* [Add Screen Shake On Hit](https://gitlab.com/OriginsProject/originspublic/-/issues/117). Toggle this effect off or on using the `Toggle Hit Shake` command in your Commands Tab.
* [Damage Numbers](https://gitlab.com/OriginsProject/originspublic/-/issues/116)
* [Hit Animation](https://gitlab.com/OriginsProject/originspublic/-/issues/115)
* [Hit Sounds](https://gitlab.com/OriginsProject/originspublic/-/issues/114). This is tied to your sound settings. Turn sounds on to enable this sound effect. Lowered the volumes for several things, so it shouldn't be BLARING like it has in the past.

### Bug Fixes
* [Steel Wing Errors](https://gitlab.com/OriginsProject/originspublic/-/issues/113)
* [Wing Attack Errors](https://gitlab.com/OriginsProject/originspublic/-/issues/112)

## Version 1.74.1

### New Features
* [Remove $ and XP Debuff from Rebattling](https://gitlab.com/OriginsProject/originspublic/-/issues/109)
* [+EXP & +Money Events](https://gitlab.com/OriginsProject/originspublic/-/issues/111) Hosted by a GM!

### Bug Fixes
* [Bugcatcher Abe has Invisible Pokemon](https://gitlab.com/OriginsProject/originspublic/-/issues/110)

## Version 1.72.1
### New Features
* [TM Seller](https://gitlab.com/OriginsProject/originspublic/-/issues/103). Please visit any Mart and look for the TM Seller Helen!
* [Update BYOND Version to 512.1526](https://gitlab.com/OriginsProject/originspublic/-/issues/108)

### Bug Fixes
* [Powders Not Removing From Battlefields](https://gitlab.com/OriginsProject/originspublic/-/issues/106)

## Version 1.70.3
### Bug Fixes
* [Unable to Move](https://gitlab.com/OriginsProject/originspublic/-/issues/104)

### Other
* [Rename master Branch to main for Public and Private Repo](https://gitlab.com/OriginsProject/originspublic/-/issues/105)

## Version 1.70.1
### New Features:
* [Ability to But TMs](https://gitlab.com/OriginsProject/originspublic/-/issues/103)


### Bug Fixes:
* [Money Doubled Due to Team Status](https://gitlab.com/OriginsProject/originspublic/-/issues/102)


## Version 1.69.1
### New Features:
* [Move Relearner](https://gitlab.com/OriginsProject/originspublic/-/issues/101) Visit the new NPC that is available in all Pokemon Centers!

### Bug Fixes: 
* [Pokemon Information Card Showing Null](https://gitlab.com/OriginsProject/originspublic/-/issues/100)
## Version 1.68.4
### Bug Fixes:
* [Leech Life Errors](https://gitlab.com/OriginsProject/originspublic/-/issues/98)

## Version 1.68.3
### Bug Fixes:
* [Density Tile Blocking Path](https://gitlab.com/OriginsProject/originspublic/-/issues/97)

## Version 1.68.2

### New Features:
* [A Cut Above The Rest Quest](https://gitlab.com/OriginsProject/originspublic/-/issues/89). Talk to Bill to start the quest!
* [Cut Ability](https://gitlab.com/OriginsProject/originspublic/-/issues/76)
* [Route 9 Spawns](https://gitlab.com/OriginsProject/originspublic/-/issues/81) New Day/Night Spawns! [Route 9 Wiki Page](https://gitlab.com/OriginsProject/originspublic/-/wikis/Route-9)
* [Cut Tree Animation](https://gitlab.com/OriginsProject/originspublic/-/issues/88)
* [Map Route 9](https://gitlab.com/OriginsProject/originspublic/-/issues/90)
* [Map Route 10](https://gitlab.com/OriginsProject/originspublic/-/issues/91)
* [Route 10 Spawns](https://gitlab.com/OriginsProject/originspublic/-/issues/92) New Day/Night Spawns! [Route 10 Wiki Page](https://gitlab.com/OriginsProject/originspublic/-/wikis/Route-10)
* [Route 10 Trainers](https://gitlab.com/OriginsProject/originspublic/-/issues/93)
* [Route 9 Trainers](https://gitlab.com/OriginsProject/originspublic/-/issues/95)

### Bug Fixes:
* [Daycare Lagy Dialogue Bug](https://gitlab.com/OriginsProject/originspublic/-/issues/94)
* [Wild Pokemon Evolve Trigger on Spawn](https://gitlab.com/OriginsProject/originspublic/-/issues/96) 


## Version 1.59.5

### Bug Fixes:
* [PC Not Working](https://gitlab.com/OriginsProject/originspublic/-/issues/85)
* [Cant Evolve After Completing Quest](https://gitlab.com/OriginsProject/originspublic/-/issues/86)

## Version 1.59.3

### Bug Fixes:
* [Quests - Cannot Sendout Pokemon](https://gitlab.com/OriginsProject/originspublic/-/issues/84)

## Version 1.59.2

### Bug Fixes:
* [Discord Login P.ckey](https://gitlab.com/OriginsProject/originspublic/-/issues/82)
* [Wing Attack Movement](https://gitlab.com/OriginsProject/originspublic/-/issues/83)

## Version 1.59.0

### New Features:
* [Contributor System](https://gitlab.com/OriginsProject/originspublic/-/issues/75). See discord announcements for details on needed pixel art!

### Bug Fixes:
* [Updated Given Setup Procedure for GitLab](https://gitlab.com/OriginsProject/originspublic/-/issues/69]

## Version 1.58.1

### Bug Fixes
* [Face Icon Debug Messages](https://gitlab.com/OriginsProject/originspublic/-/issues/73)

## Version 1.58.0

### New Features
* [Effect Spore Ability Added](https://gitlab.com/OriginsProject/originspublic/-/issues/70)
* [Pokemon move learnsets are now Gen 3](https://gitlab.com/OriginsProject/originspublic/-/issues/74)
* [Poison Point Ability Added](https://gitlab.com/OriginsProject/originspublic/-/issues/71)
* [Savefile format refactoring](https://gitlab.com/OriginsProject/originspublic/-/issues/72). This one took quite a bit of time, but results in a 10-20x reduction in size of savefiles, ultimately resulting in performance increases.

## Version 1.55.0

### New Features
* [New Movement System](https://gitlab.com/OriginsProject/originspublic/-/issues/68). This replaces the repeat macro system, which leads to significant performance improvements.

## Version 1.54.8



### Bug Fixes
* [**Auto Battle Error.**](https://gitlab.com/OriginsProject/originspublic/-/issues/67) **This fix was huge and should reduce or eliminate the server freezes that we have been experiencing on the server. Much thanks to Luber for helping out this this one!**
* [Pokedollars label wrong after battle](https://gitlab.com/OriginsProject/originspublic/-/issues/15)
* [Battle Damage Error](https://gitlab.com/OriginsProject/originspublic/-/issues/66)
* [Tri Attack Errors](https://gitlab.com/OriginsProject/originspublic/-/issues/63)
* [Pokemon Information Error](https://gitlab.com/OriginsProject/originspublic/-/issues/64)
* [Fury Swipes Error](https://gitlab.com/OriginsProject/originspublic/-/issues/65)
* [Powders Performance](https://gitlab.com/OriginsProject/originspublic/-/issues/61) This will also lead to future improvements because I established object pooling with this one. Basically, we can reuse objects as needed rather than creating / deleting new ones. This is less costly for processing.
* [Leer Errors](https://gitlab.com/OriginsProject/originspublic/-/issues/41)

## Version 1.54.0

### New Features:
* [Adjusted movement thresholds](https://gitlab.com/OriginsProject/originspublic/-/issues/56) Overall speed "buff"
* [Make movement more consistent](https://gitlab.com/OriginsProject/originspublic/-/issues/45)
* [Add Dig as a reward for robbed house quest](https://gitlab.com/OriginsProject/originspublic/-/issues/59) See Hinashou for reward if you have already been through the robbed house.

### Bug Fixes
* [Performance Fixes](https://gitlab.com/OriginsProject/originspublic/-/issues/57)
    * New Who command
    * To view Pokemon Move PP & Experience - click on the pokemon
    * To view money - click on your trainer.

## Version 1.53.0

### New Features:
* [Players FPS settings will now save and persist](https://gitlab.com/OriginsProject/originspublic/-/issues/49)

### Bug Fixes
* [Speed stat is always base stat](https://gitlab.com/OriginsProject/originspublic/-/issues/50)
* [Cannot move while using rollout or moves like it](https://gitlab.com/OriginsProject/originspublic/-/issues/47)
* [Daycare not working](https://gitlab.com/OriginsProject/originspublic/-/issues/50)
## Version 1.52.1

### Bug Fixes:
* [Debug messages when moving on water](https://gitlab.com/OriginsProject/originspublic/-/issues/46)

## Version 1.52.0
### New Features:
* Added ability for player to change their fps (up to 100). Default is now 100 on the client.

### Bug Fixes
* [Leer crash](https://gitlab.com/OriginsProject/originspublic/-/issues/40)
* [Fury Swipes Crash](https://gitlab.com/OriginsProject/originspublic/-/issues/41)
* [SS Anne Captain Door sent players to void](https://gitlab.com/OriginsProject/originspublic/-/issues/42)
* [Battlemap performance bug](https://gitlab.com/OriginsProject/originspublic/-/issues/43)

## Version 1.51.1
### Bug Fixes 
* [Tunnel from Vermilion City teleports you under door](https://gitlab.com/OriginsProject/originspublic/-/issues/38)

## Version 1.51.0

### New Features:
* [Added dynamic party generation for gym leaders.](https://gitlab.com/OriginsProject/originspublic/-/issues/23)
    * Gym Leader's pokemon level will scale based on the highest level pokemon you bring in to battle.
    * The number of pokemon the leader will bring into battle will be determined by the number you bring to battle as well. 

*Gym Leader pokemon level and party count have defaults. (Example: Misty will at minimum bring a level 18 Staryu and a level 21 Starmie into battle and scale up accordingly)*
### Bug Fixes: 
* [Unable to enter Cave](https://gitlab.com/OriginsProject/originspublic/-/issues/37)

## Version 1.50.3
### Bug Fixes:
* [Mt. Moon Ladders not showing](https://gitlab.com/OriginsProject/originspublic/-/issues/30)
* [Hitting pokemon with physical moves no longer fails to give experience sometimes.](https://gitlab.com/OriginsProject/originspublic/-/issues/29)
* [Watchers able to enter battlefield](https://gitlab.com/OriginsProject/originspublic/-/issues/31)

## Version 1.50
### New Features:

* Discord notifications for game events
* Dynamic day/night system.
* Added an automatic update system.
* Added a full GitLab Continuous Integration / Continuous Delivery Pipeline to the project.
* Dynamic vegetation system
* Dynamic water vegetation system
* Easter egg when leaving Viridian Forest for the first time
* Dynamic shadow generation system
* Change screen size - 32x32 or Stretch to Fit

### Bug Fixes:

* Hitting pokemon with physical moves no longer fails to give experience sometimes.
* Base icon loading correctly no matter what option you choose [Issue 17](https://gitlab.com/OriginsProject/originspublic/-/issues/17)

### Enhancements:

* Remapping of the entire game.
* Added additional parameters to the map instancing system.
* HUD icons are more consistent (Example: Tackle button looks the same for all Pokemon)
* Removed Quit command as a file option in the dropdown menu
* Updated links on the right

## Version 1.10
- Stun Spore Adjustments
    - Nerf sleep duration sleep cycle from 4-6 seconds to around 1-2 seconds
- HP Meter Overlay Bug
    - Fixed a bug with overlays in HP Meter that caused huge overlay lists saving to Pokemon objects.
    - Fixed an issue with persistent overlay in Owned icon for NPC controlled Pokemon.
- Added Team Indicators
    - When battling, the HP Bar of the Pokemon will change to indicate which team that they are on. This is to avoid any confusion with duplicate Pokemon and team battles.
    - Team 1: Green
    - Team 2: Blue
    - Team 3: Yellow
    - Team 4: Purple
- HP Meter Animation
    - HP Meter for Pokemon will now start flashing Red at 20% health or lower.
-Combat Logging System
    - Information to be displayed to the combat log regarding damage.
    - Allows players to understand what is happening in the battle.
    - Allows players to crunch numbers to come up with the best combos, etc.
    - Provides more information to players to provide balance feedback.
    - Provides information on the effectiveness of the attack and will display if a critical hit landed or not.
    - Example: 
Hinashou: Charmander, use Ember!
Weedle has taken 28 damage from Charmander's Ember!
It's super effective!
Critical Hit!

- Fixed overlay bug where overlays persisted on Pokemon when relogging.
- Fixed multiple bugs with Sing
    - Lasts for 3 seconds
    - 75% chance to put Pokemon within 1 tile to sleep. Triggers every 2/10 of a second.
    - Move is cancelled if a Pokemon is sucessfully put to sleep.
    - Can sleep multiple targets if they are all 1 tile away and are individually successful on the 75% roll.
- Added or updated icon states for all Pokemon.
- Added firing state when these moves are used: 
    - Vine Whip
    - Water Pulse
- When you send out a Pokemon, it will pull the latest icon file. This will make any icon updates automatic in the future.
- Added standing states for Charmeleon and Charizard where their flame will animate like it does with Charmander.
- Reworked Thrash
    - 12 second cooldown
    - Goes through each tile around the Pokemon, either kicking, scratching, punching, or biting anyone in that area.
    - User is confused after using Thrash
- Increased speed of Bite icon and Pound icon
- Fixed bug where double kick icon was reversed on North and South icon states.
- Added various move buttons
- Reworked Double-Edge
    - 16 second cooldown
    - Pokemon quickly lunges forward 4 tiles, then dashes back those same 4 tiles. Any Pokemon in path takes damage, and the Pokemon performing Double-Edge will take 15% of the damage it dealt.
    - Added combat display message to the Combat Logging System for recoil damage. Example: 
Raticate has taken 12 damage from the recoil!
- Reworked Dragon Breath
    - New Icon
    - 15 second cooldown
    - Immediately pushes back and damages any Pokemon 4 tiles in front of user.
    - Channels Dragon Breath for 1.1 seconds, any Pokemon that walks in the line of attack will be thrown and damaged.
- Reworked Double Kick
    - Icon now reflects area of effect. 
    - User will strike 3 tiles in front of them, going left to right. After a brief delay, it will strike again.
- Fixed bug where Attacker's Special Defense stat was used instead of Defender's Special Attack stat when calculating damage.
- Reworked Dragon Rage 
    - New Icon
    - Increased cooldown from 11 seconds to 16 seconds
    - Larger icon, 2x2
- Added Dragonclaw to Charizard's moveset. Available at level 1.
- Slight update to Dragonclaw icon.
- Bug fix for Charizards "Pushback" icon state. Directions were reversed.
- Body Slam rework
    - 12 second cooldown
    - User charges in front of them, pulling any Pokemon 2 tiles in front of them to the user. Continues to thrust with any grabbed Pokemon.
    - If a Pokemon was grabbed during the charge, the user will throw the target in a random direction and damage them.
- Fixed a bug with birdflocks generating on outside maps. Previously only spawned on map 1. 
- Performance improvements for birdflocks.
- Removed auto-reboot on server.
- Twister rework
    - New Icon
    - Performance improvements
    - No longer tracks a target
    - 64x96 area
- Increased speed in which Pushback effect takes place. Previously used the Pokemon's Speed stat to determine how fast the pushback worked. Now it is a flat rate.